# Objetivos

- IaaS
- Openstack
  - Teoria
    - Openstack morreu?
    - Casos de Uso
    - Componentes
    - Porque VMs e não containers?
  - DevStack
  - Cliente (openstack) e Painel (Horizon)
  - Domínios, projetos, usuários e permissões
  - Redes: tenant e provider
  - Flavors
  - Imagens
  - Máquinas Virtuais
  - Discos

# IaaS

Infraestrutura como Serviço, em inglês Infrastructure as a Service (IaaS) é um modelo de serviço no qual os recursos computacionais são fornecidos por um determinado provedor. Os fornecedores de IaaS fornecem muitos serviços como armazenamento, redes e virtualização liberando os consumidores do fardo de manter o hardware de uma infraestrutura localmente.

Os serviços de IaaS podem estar em uma nuvem pública (compartilhando o hardware com outros usuários), uma nuvem privada (sem compartilhamento de recursos) ou uma núvem híbrida, combinando os dois modelos.

Nesse tipo de serviço é comum existirem APIs para facilitar a criação de recursos de forma automatizada.

# Openstack

Openstack é uma plataforma livre e de código aberto para computação em nuvem. É um dos três projetos de código aberto mais ativos do mundo, junto com o kernel do Linux e o navegador Chromium.

- https://www.openstack.org/software/

Assim como a maioria dos softwares livres, o Openstack é mantido por muitas empresas de forma simultânea, cada empresa possui sua própria versão derivada da versão "community" (chamada de upstream).

Algumas da empresas envolvidas no Openstack:

- Red Hat
- Oracle
- Mirantis
- Canonical
- IBM
- HP (deixou o projeto)
- SUSE (deixou o projeto)

[releases](https://releases.openstack.org/)

## Openstack Morreu?

Assim como os Mainframes, Cobol e Java, não o Openstack não está morrendo.

- https://ubuntu.com/blog/openstack-is-dead

## Casos de Uso

- https://www.openstack.org/use-cases/

## Componentes

- https://www.openstack.org/software/

## Porque VMs e não Containers

Existem alguns casos em que máquinas virtuais são mais vantajosas que containeres:

- Persistência de dados ocorre em vários lugares;
- Fornecedor não suporta containers;
- Arquitetura diferente do CPU da máquina hospedeira;
- Maior isolamento;
- Teste de drivers específicos;

# DevStack

https://www.openstack.org/software/project-navigator/deployment-tools

O [DevStack](https://docs.openstack.org/devstack/zed/) é um conjunto de scripts utilizados para criar um ambiente completo de Openstack através do git. Este ambiente é muito mais leve e simples que uma instalação de produção, e normalmente é utilizado para testes e estudo.

### Configuração e Instalação

Para a versão **zed** do Openstack, uma máquina com Ubuntu 22.04 foi utilizada.

Como o usuário `root`, executar os seguintes comandos:

```bash
apt-get update && apt-get install -y git
useradd -s /bin/bash -d /opt/stack -m stack
chmod +x /opt/stack
echo "stack ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/stack
sudo -u stack -i
```

Como o usuário `stack` executar os seguintes comandos:

```bash
git clone https://opendev.org/openstack/devstack
cd devstack
git checkout stable/zed
cat > local.conf <<'EOF'
[[local|localrc]]
ADMIN_PASSWORD=openstack
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD
enable_plugin heat https://opendev.org/openstack/heat stable/zed
enable_service s-proxy s-object s-container s-account
EOF
```

Com o arquivo definido, iniciar a instalação:

```bash
./stack.sh
```

Ao finalizar a instalação, podemos começar a interar com o Openstack através de variáveis de ambiente que são configuradas pelo arquivo `openrc`:

```bash
source openrc
```

# Cliente (openstack) e Painel (Horizon)

Todos os serviços do Openstack possuem seus próprios clientes, cada qual com sua própria sintaxe. Felizmente existe um cliente unificado chamado `openstack` que padroniza e centraliza a execução dos comandos que conversarão com os mais diversos componentes. Em algumas poucas exceções o cliente de determinados componentes precisarão ser utilizados (por exemplo a criação de "shares").

O cliente do Openstack é bastante versátil e fornece uma sintaxe padronizada para a grande maioria dos comandos:

```bash
openstack <tipo> <ação> [parâmetros] [nome]
```

Por exemplo:

```bash
openstack image create --file ~/debian-11-genericcloud-amd64-20230124-1270.qcow2 debian-11
```

Caso tenha dúvida em algum comando, podemos verificar a saída da ajuda. Existem algumas formas e dependendo da versão a exibição pode ser diferente entre estes dois comandos:

```bash
openstack help server create
# ou
openstack server create --help
```

Para situações mais simples, podemos simplesmente omitir os parâmetros obrigatórios e receber uma saída útil para verificações:

```bash
openstack image create 
# output:
# usage: openstack image create [-h] [-f {json,shell,table,value,yaml}] [-c COLUMN] [--noindent] [--prefix PREFIX] [--max-width <integer>] [--fit-width] [--print-empty] [--id <id>]
#                               [--container-format <container-format>] [--disk-format <disk-format>] [--min-disk <disk-gb>] [--min-ram <ram-mb>] [--file <file> | --volume <volume>]
#                               [--force] [--progress] [--sign-key-path <sign-key-path>] [--sign-cert-id <sign-cert-id>] [--protected | --unprotected]
#                               [--public | --private | --community | --shared] [--property <key=value>] [--tag <tag>] [--project <project>] [--import] [--project-domain <project-domain>]
#                               <image-name>
# openstack image create: error: the following arguments are required: <image-name>
```

O formato padrão de exibição das informações do Openstack é em tabela:

```bash
openstack image list
# output:
# +--------------------------------------+---------------------------------+--------+
# | ID                                   | Name                            | Status |
# +--------------------------------------+---------------------------------+--------+
# | f05c04f2-1071-4e1f-b1c9-58f31f97c094 | Fedora-Cloud-Base-36-1.5.x86_64 | active |
# | 71bbd221-8517-4117-9eb9-e7741c0697ed | cirros-0.5.2-x86_64-disk        | active |
# +--------------------------------------+---------------------------------+--------+
```

Podemos modificar o resultado trocando para JSON ou YAML, o que facilita o processamento destas informações por linguagens de programação:

```bash
openstack image list -f json
# output:
# [
#   {
#     "ID": "f05c04f2-1071-4e1f-b1c9-58f31f97c094",
#     "Name": "Fedora-Cloud-Base-36-1.5.x86_64",
#     "Status": "active"
#   },
#   {
#     "ID": "71bbd221-8517-4117-9eb9-e7741c0697ed",
#     "Name": "cirros-0.5.2-x86_64-disk",
#     "Status": "active"
#   }
# ]
```

Muitas vezes a saída dos comandos são muito longas e acabam sendo muito difíceis de ler, para isso podemos utilizar o subcomando `--fit-width` ou `--max-width=<N>`:

```bash
openstack image show f05c04f2-1071-4e1f-b1c9-58f31f97c094 --max-width=80
# +------------------+-----------------------------------------------------------+
# | Field            | Value                                                     |
# +------------------+-----------------------------------------------------------+
# | checksum         | 7f7cdad25b77f232078bf454c39529d3                          |
# | container_format | bare                                                      |
# | created_at       | 2023-02-25T01:04:26Z                                      |
# | disk_format      | qcow2                                                     |
# | file             | /v2/images/f05c04f2-1071-4e1f-b1c9-58f31f97c094/file      |
# | id               | f05c04f2-1071-4e1f-b1c9-58f31f97c094                      |
# | min_disk         | 0                                                         |
# | min_ram          | 0                                                         |
# | name             | Fedora-Cloud-Base-36-1.5.x86_64                           |
# | owner            | 35f37247834742cfb58946b13265e4a6                          |
# | properties       | hw_rng_model='virtio', os_hash_algo='sha512', os_hash_val |
# |                  | ue='e1b41fe4e7e911c58fc90cceb30da8adfb8302e5935f2edad8b11 |
# |                  | 72f1a52c6ba3faa1f23d170aeef1f062f77bf7838bc9e8a6dd3c58dfe |
# |                  | 7278596c4899338620', os_hidden='False',                   |
# |                  | owner_specified.openstack.md5='',                         |
# |                  | owner_specified.openstack.object='images/Fedora-Cloud-    |
# |                  | Base-36-1.5.x86_64', owner_specified.openstack.sha256=''  |
# | protected        | False                                                     |
# | schema           | /v2/schemas/image                                         |
# | size             | 448266240                                                 |
# | status           | active                                                    |
# | tags             |                                                           |
# | updated_at       | 2023-02-25T01:04:31Z                                      |
# | virtual_size     | 5368709120                                                |
# | visibility       | public                                                    |
# +------------------+-----------------------------------------------------------+
```

## Variáveis de Ambiente

Para interagir com o Openstack podemos especificar parâmetros no próprio cliente ou utilizar variáveis de ambiente, a segunda forma é muito mais comum. Após carregar as variáveis do arquivo `openrc` podemos listá-las da seguinte forma no nosso ambiente:

```bash
env | grep OS_
# output:
# OS_PASSWORD=openstack
# OS_IDENTITY_API_VERSION=3
# OS_TENANT_NAME=demo
# OS_USER_DOMAIN_ID=default
# OS_REGION_NAME=RegionOne
# OS_AUTH_URL=http://10.42.0.15/identity
# OS_USERNAME=demo
# OS_AUTH_TYPE=password
# OS_PROJECT_NAME=demo
# OS_PROJECT_DOMAIN_ID=default
# OS_CACERT=
# OS_VOLUME_API_VERSION=3
```

Estas variáveis representam nosso usuário, a senha, o ponto de autenticação, o projeto padrão entre outras coisas. No nosso caso, apenas trocar o valor de `OS_USERNAME` para `admin` já nos torna administrador do Openstack, por exemplo:

```bash
openstack domain list
# output:
# You are not authorized to perform the requested action: identity:list_domains. (HTTP 403) (Request-ID: req-fba7f20a-ca8d-4aca-9a04-20bac073e265)
export OS_USERNAME=admin
openstack domain list
# output:
# <exibição dos domínios>
```

É uma boa prática criar arquivos de ambiente para cada usuário, ou mesmo para ambientes diferentes.

https://docs.openstack.org/ocata/user-guide/common/cli-set-environment-variables-using-openstack-rc.html

# Domínios, projetos, usuários e permissões

```bash
openstack domain create d1
```

```bash
openstack project create --domain d1 p1
```

```bash
openstack user create --domain d1 --password openstack operator1
``` 

Por padrão existem apenas duas roles que podemos utilizar no Openstack reconhecida por todos os serviços, `member` e `admin`. Adicionando um usuário como admin ele se torna administrador do cluster por completo e não somente do projeto ou domínio específico. A permissão do tipo `member` permite a criação de redes, imagens, máquinas virtuais e outros serviços que estão dentro apenas dos projetos.

```bash
openstack role add --user operator1 --domain d1 admin
# openstack role add --user operator1 --domain d1 member
```

Para visualizar as permissões:

```bash
openstack role assignment list --names
```

Com o usuário criado podemos baixar seu arquivo de configuração pelo painel do Horizon ou criá-lo manualmente:

```bash
bash > operator1-d1.sh <<'EOF'
export OS_REGION_NAME=RegionOne
export OS_INTERFACE=public
export OS_AUTH_URL=http://10.42.0.15/identity
export OS_USERNAME=operator1
export OS_USER_DOMAIN_NAME=d1
export OS_PROJECT_NAME=p1
export OS_PASSWORD=openstack
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_ID=cda0e77a1b4b421c96075d6d1d77b2e5
export OS_PROJECT_DOMAIN_ID=35e96d353f39439fb944194ff14e08a3
export PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w(operator1-d1)\$ '
EOF
```

Para carregá-lo, basta executar:

```bash
source operator1-d1.sh
```

## Exercício

- Criar um domínio chamado `mit`
- Dentro do domínio `mit` criar um projeto chamado `iaas`
- Criar dois usuários dentro do domínio `mit`, dentro do projeto `iaas`:
  - Um deles será o administrador do domínio e se chamará `mit-operator` (admin)
  - O outro será o desenvolvedor e se chamará `mit-developer` (member)
- Criar o arquivo de ambiente de ambos os usuários e testá-los.

**Resultado Esperado**

- O usuário `mit-operator` ao executar `openstack project list` deve visualizar todos os projetos do cluster
- O usuário `mit-developer` ao executar `openstack project list` deve visualizar apenas o projeto `iaas`

# Redes

O serviço responsável pela criação e gerenciamento de redes é o **Neutron**.

## Tenant e Provider

As redes `tenant` são as redes privadas, podem ser criadas pelos usuários comuns e não afetam a rede física, possuem seu próprio range, seu servidor DHCP e etc. Já a rede do tipo `provider` utiliza-se da rede física e dos serviços nela presente, como o próprio DHCP e os ips disponíveis para alocação.

```bash
openstack floating ip create public
```

# Flavors

Flavors, ou sabores em português, são as configurações pré-definidas de máquinas que podemos escolher durante a criação de um servidor. O DevStack cria alguns flavors para nós:

```bash
openstack flavor list
# output:
# +---------------+-------+------+-----------+-------+-----------+
# | Name          |   RAM | Disk | Ephemeral | VCPUs | Is Public |
# +---------------+-------+------+-----------+-------+-----------+
# | m1.tiny       |   512 |    1 |         0 |     1 | True      |
# | m1.small      |  2048 |   20 |         0 |     1 | True      |
# | m1.medium     |  4096 |   40 |         0 |     2 | True      |
# | m1.large      |  8192 |   80 |         0 |     4 | True      |
# | m1.nano       |   128 |    1 |         0 |     1 | True      |
# | m1.xlarge     | 16384 |  160 |         0 |     8 | True      |
# | m1.heat_int   |   512 |   10 |         0 |     1 | True      |
# | m1.micro      |   192 |    1 |         0 |     1 | True      |
# | cirros256     |   256 |    1 |         0 |     1 | True      |
# | ds512M        |   512 |    5 |         0 |     1 | True      |
# | ds1G          |  1024 |   10 |         0 |     1 | True      |
# | ds2G          |  2048 |   10 |         0 |     2 | True      |
# | ds4G          |  4096 |   20 |         0 |     4 | True      |
# | m1.heat_micro |   128 |    1 |         0 |     1 | True      |
# +---------------+-------+------+-----------+-------+-----------+
```

## Images

O serviço responsável pela criação e gerenciamento de imagens é o **Glance**.

https://docs.openstack.org/image-guide/obtain-images.html

## user data

```bash
#!/bin/bash

echo fedora | passwd fedora --stdin
```

## cloud-init

```
#cloud-config
password: openstack
chpasswd: {expire: False}

package_update: true
packages:
 - apache2
 - mariadb-server
   
runcmd:
- systemctl enable --now mysql apache2
```

# Máquinas Virtuais

O serviço responsável pela criação e gerenciamento de máquinas virtuais é o **Nova**.

# Discos

O serviço responsável pela criação e gerenciamento de discos é o **Cinder**.

# Object Storage

Por padrão o Ceph utiliza o **Swift** como object storage (também conhecido como "bucket"), capaz de armazenar petabytes de dados de forma escalável e redundante. O swift possui seu próprio protocolo mas também é relativamente compatível com S3.

O swift chama o local de armazenamento dos arquivos de "container" apesar do nome "bucket" ser mais comum, tratando-se deste storage estes termos são intercambiáveis.

```bash
openstack container create configurations

cat > ports.conf <<EOF
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 80

<IfModule ssl_module>
        Listen 443
</IfModule>

<IfModule mod_gnutls.c>
        Listen 443
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

openstack object create configurations ports.conf
openstack object list configurations
cd /tmp
openstack object save configurations ports.conf 
cat ports.conf
```

## Utilizando o curl

Utilizar o `curl` para fazer consultas e baixar arquivos do Swift é relativamente simples e facilita bastante a utilização de arquivos de configuração externos às máquinas. Além da consulta podemos executar todas as operações que normalmente faríamos através do cliente `openstack`, como enviar e deletar arquivos.

Para isso precisaremos:

- Criar um token
- Obter a "conta" do bucket
- Descobrir o endereço do swift
- Executar o `curl` com o cabeçalho `X-Auth-Token`

```bash
openstack token issue
# output:
# +------------+-----------------------------------------------------------------+
# | Field      | Value                                                           |
# +------------+-----------------------------------------------------------------+
# | expires    | 2023-02-26T04:11:31+0000                                        |
# | id         | gAAAAABj-                                                       |
# |            | s3jQorDC4WTsxCu6Pr375PGy5RNRwZPmTVZyzzCvtL5mWImRbXmkkl6Hs-dCgq0 |
# |            | vEdczLdAw1Amjie903PgLPEW_YZ0c-                                  |
# |            | 8kiNLbbQbRgsm6j2Au06Vve6sSxddYLFwCoqSt1liIIe3bFTocUfBksazyafge2 |
# |            | UYVbtdeMoOdXuFdRVU                                              |
# | project_id | a20a6ced757e4c0f9b7048154654d9fe                                |
# | user_id    | 80b554bf14b24e25806094b3810f4ec8                                |
# +------------+-----------------------------------------------------------------+
```

Copiar o token e salvar em uma variável para melhor conveniência, o token não possui quebras de linhas.

```bash
export TOKEN='gAAAAABj...2OnQ77POMschrNk'
```

Obter a conta do bucket:

```bash
openstack container show configurations
# output:
# +----------------+---------------------------------------+
# | Field          | Value                                 |
# +----------------+---------------------------------------+
# | account        | AUTH_a20a6ced757e4c0f9b7048154654d9fe |
# | bytes_used     | 334                                   |
# | container      | configurations                        |
# | object_count   | 1                                     |
# | storage_policy | Policy-0                              |
# +----------------+---------------------------------------+
``` 

Normalmente o swift possui um endereço como `swift.openstack.example.com` o que torna a utilização muito mais fácil, mas em alguns para acessar o swift necessitaremos de um endereço IP e uma porta, como é o caso do DevStack. Para descobrir onde o swift está, basta verificarmos os `endpoints` do Openstack:

```bash
openstack endpoint list -c 'Service Name' -c 'Service Type' -c URL
# output:
# +--------------+----------------+-----------------------------------------------+
# | Service Name | Service Type   | URL                                           |
# +--------------+----------------+-----------------------------------------------+
# | glance       | image          | http://10.42.0.15/image                       |
# | placement    | placement      | http://10.42.0.15/placement                   |
# | nova_legacy  | compute_legacy | http://10.42.0.15/compute/v2/$(project_id)s   |
# | cinder       | block-storage  | http://10.42.0.15/volume/v3/$(project_id)s    |
# | swift        | object-store   | http://10.42.0.15:8080/v1/AUTH_$(project_id)s |
# | heat-cfn     | cloudformation | http://10.42.0.15/heat-api-cfn/v1             |
# | nova         | compute        | http://10.42.0.15/compute/v2.1                |
# | heat         | orchestration  | http://10.42.0.15/heat-api/v1/$(project_id)s  |
# | cinderv3     | volumev3       | http://10.42.0.15/volume/v3/$(project_id)s    |
# | neutron      | network        | http://10.42.0.15:9696/networking             |
# | swift        | object-store   | http://10.42.0.15:8080                        |
# | keystone     | identity       | http://10.42.0.15/identity                    |
# +--------------+----------------+-----------------------------------------------+
```

Um exemplo de listagem e download de arquivo através do curl:

```bash
curl -H "X-Auth-Token: $TOKEN" http://10.42.0.15:8080/v1/AUTH_a20a6ced757e4c0f9b7048154654d9fe
# configurations
curl -H "X-Auth-Token: $TOKEN" http://10.42.0.15:8080/v1/AUTH_a20a6ced757e4c0f9b7048154654d9fe/configurations
# ports.conf
```
